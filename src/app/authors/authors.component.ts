import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthorsService } from '../authors.service';
import { identifierName } from '@angular/compiler';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {


  panelOpenState = false;
  authors$:Observable<any>;
  author:string;
  id;
  /*onSubmit(){
    this.authors(['/authors', this.author]);
  }*/
  books: object[] =  [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}]
  constructor(private route: ActivatedRoute,private authorsservice:AuthorsService) {  }

  addAuthor(){
    /*this.authorsservice.addAuthor(this.newAuthor);
*/
  }
  newAuthor(newAuthor: any) {
    throw new Error("Method not implemented.");
  }
  ngOnInit() {
    /* this.books = this.booksservice.getBooks().subscribe(
       (books)=>this.books = books);*/
       this.authors$ = this.authorsservice.getAuthor()
       this.authorsservice.addAuthor();
       this.id = this.route.snapshot.params.id;

   }

}

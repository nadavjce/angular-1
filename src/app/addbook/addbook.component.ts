import { Component, OnInit } from '@angular/core';
import { BooksService } from '../books.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Identifiers } from '@angular/compiler';

@Component({
  selector: 'app-addbook',
  templateUrl: './addbook.component.html',
  styleUrls: ['./addbook.component.css']
})
export class AddbookComponent implements OnInit {
  title:string;
  author:string; 
  id:string;
  isEdit:boolean = false;
  buttonText:string = "Add book";

  constructor(private booksservice:BooksService, private router:Router ,private route:ActivatedRoute) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    console.log(this.id);
    if(this.id){
      this.isEdit =true;
      this.buttonText = "Update book"
      this.booksservice.getBook(this.id).subscribe(
        book => {
          console.log(book.data().author)
          console.log(book.data().title)
          this.author = book.data().author;
          this.title = book.data().title; 
    }
      )
  }
  }

  onSubmit(){
    if(this.isEdit){
      this.booksservice.updateBook(this.id,this.title,this.author);
    }else{
      this.booksservice.addBook(this.title, this.author)
    }
    this.router.navigate(['/books']);
  }
}

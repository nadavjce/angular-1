
import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class BooksService {
  
  //books:any = [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}];
  getBooks():Observable<any[]>{
    return this.db.collection('books').valueChanges({idField:'id'});
  }

  addBook(title:string, author:string){
    const book = {title:title, author:author}
    this.db.collection('books').add(book);
  }

  deleteBook(id:string)
  {
    this.db.doc(`books/${id}`).delete();
  }

  getBook(id:string):Observable<any>{
    return this.db.doc(`books/${id}`).get();
  }
  
  updateBook(id:string,title:string, author:string){
{
  this.db.doc(`books/${id}`).update({
    title:title,
    author:author
  })
  
}
  }
  /*getBooks(){
    //return this.books; adds all books
    //setInterval(()=> this.books,1000)
    const booksObservable = new Observable(
      observer => {
        setInterval(
          ()=>observer.next(this.books),3000)
      }
    )
    return booksObservable;
  }*/
/*addBooks(){
  setInterval(()=>this.books.push({title:'A new book', author:'New author'})
  ,2000)
}*/

  constructor(private db:AngularFirestore) { }

}
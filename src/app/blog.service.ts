import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {Posts} from './interfaces/posts';
import {Users} from './interfaces/users';
import { AngularFirestore,AngularFirestoreModule, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  private APIPosts = "https://jsonplaceholder.typicode.com/posts/";
  private APIUsers = "https://jsonplaceholder.typicode.com/users/";

    constructor(private http:HttpClient,private db:AngularFirestore)  { }
    deletePost(id:string)
  {
    this.db.doc(`post/${id}`).delete();
  }
    
  getBlogPost():Observable<Posts[]>{
  return this.http.get<Posts[]>(`${this.APIPosts}`);
   }
   getUserPost(): Observable<Users[]>{
    return this.http.get<Users[]>(`${this.APIUsers}`);
   }
   addPost(body:string,author:string,title:string){
    const posts = {body:body, author:author,title:title}
    this.db.collection('posts').add(posts);
  }

}

/*
import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import { HttpClient } from '@angular/common/http';
import {Posts} from './interfaces/posts';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  private API = "https://jsonplaceholder.typicode.com/posts/";
  addPost: any;

  constructor(private http:HttpClient) { }
  getPosts(): Observable<Posts>{
  return this.http.get<Posts>(`${this.API}`);
}
}
*/



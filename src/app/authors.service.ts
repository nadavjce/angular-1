import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthorsService {
  addAuthor() {
    setInterval(()=>this.author.push({author:'New author'})
    ,4000);
  }
  author:any = [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}];

  getAuthor(){
    const authorObservable = new Observable(
      observer => {
        setInterval(
          ()=>observer.next(this.author),4000)
      }
    )
    return authorObservable;
  }
  
  constructor() { }
  
}

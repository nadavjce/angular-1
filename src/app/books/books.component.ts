import { Component, OnInit } from '@angular/core';
import { BooksService } from '../books.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  panelOpenState = false;
  //books: any;
  books$:Observable<any>;
  deleteBook(id:string){
    this.booksservice.deleteBook(id);
  }

  constructor(private booksservice:BooksService) { 

  }

  ngOnInit() {
   /* this.books = this.booksservice.getBooks().subscribe(
      (books)=>this.books = books);*/
      this.books$ = this.booksservice.getBooks()
      //this.booksservice.addBooks();
  }

}
